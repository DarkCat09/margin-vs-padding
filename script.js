var body = document.body;
var wrapper = document.querySelector('.example-wrapper');
var elem = document.querySelector('.element');
var content = document.querySelector('.content');
var hint = document.querySelector('.hint');
var textarea = document.querySelector('.css-code');

const props = ['margin','border','padding'];
const sides = ['top','right','bottom','left'];

var inputs = document.querySelectorAll('input[type=range]');
inputs.forEach(elem => elem.addEventListener('input', valueChanged));

document.querySelector('input#dark-mode').addEventListener(
    'input', (ev) => checkbox(ev, body, 'dark')
);

document.querySelector('input#show-element').addEventListener(
    'input', (ev) => checkbox(ev, wrapper, 'hidden')
);

document.querySelector('input#show-css').addEventListener(
    'input', (ev) => checkbox(
        ev, textarea, 'show',
        printCss
    )
);

if (window.matchMedia &&
    window.matchMedia('(prefers-color-scheme: dark)').matches)
{
    checkbox({target: {checked: true}}, body, 'dark');
}

function valueChanged(event) {
    let el = event.target;
    let id = el.id;
    let span = `span[data-for=${id}]`;
    document.querySelector(span).innerText = el.value;

    let val = el.value + 'px';
    body.style.setProperty(`--${id}`, val);
}

function printCss() {
    let css = document.defaultView.getComputedStyle(body);
    let styles = [];
    for (let prop of props) {
        let txt = prop + ':';
        for (let side of sides) {
            let variable = `--${prop}-${side}`;
            console.log(variable);
            let val = css.getPropertyValue(variable);
            txt += (' ' + val.trim());
        }
        txt += ';'
        styles.push(txt);
    }
    let width = css.getPropertyValue(`--width`);
    let height = css.getPropertyValue(`--height`);
    styles.push(`width: ${width.trim()};`);
    styles.push(`height: ${height.trim()};`);
    textarea.value = styles.join('\n');
   elem.getComputedStyles();
}

function checkbox(ev, el, cls, cb) {
    cb = cb || function(){};
    ev.target.checked ?
        el.classList.add(cls) & cb() :
        el.classList.remove(cls);
}
